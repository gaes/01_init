# What is npm? #

npm makes it easy for JavaScript developers to share and reuse code, and it makes it easy to update the code that you're sharing.

# Commands CLI #

* sudo apt-get install npm
* npm -v
* npm init
* npm install jquery --save
* npm install bootstrap --save
* npm install
* npm list